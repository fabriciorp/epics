<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Inclusão de Produtos - Teste EPICS</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
    <div class="container-fluid mt-3">
        <div class="row justify-content-around align-items-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-sm-6">
                                <h3 class="card-title">
                                    Inclusão de Produtos
                                </h3>
                            </div>
                            <div class="col-sm-6">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb float-sm-right">
                                        <li class="breadcrumb-item">
                                            <a href="index.php" title="Home">Home</a>
                                        </li>
                                        <li class="breadcrumb-item">
                                            <a href="productsListar.php" title="Lista de Produtos">Lista de Produtos</a>
                                        </li>
                                        <li class="breadcrumb-item active">
                                            Inclusão de Produtos
                                        </li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <form role="form" action="productsGravar.php" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="acao" id="acao" value="1">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-7">
                                    <div class="form-group">
                                        <label for="nome">Nome</label>
                                        <input type="text" maxlength="50" autofocus required id="nome" name="nome" placeholder="Digite o Nome" class="form-control">
                                    </div>
                                </div>
                                <div class="col-sm-5">
                                    <div class="form-group">
                                        <label for="categoria">Categoria</label>
                                        <input type="text" maxlength="25" required id="categoria" name="categoria" placeholder="Digite a Categoria" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="cliente_id">ID do Cliente</label>
                                        <input type="number" min='1' maxlength="11" required id="cliente_id" name="cliente_id" placeholder="Digite o ID do Cliente" class="form-control">
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="valor">Valor</label>
                                        <input type="text" maxlength="17" required id="valor" name="valor" placeholder="Digite o Valor" data-thousands="." data-decimal="," data-prefix="R$ " class="form-control">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="foto">Imagem</label>
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="foto" name="foto">
                                            <label class="custom-file-label" for="foto">Selecione uma imagem</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer text-right">
                            <button type="submit" class="btn btn-primary">Cadastrar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- jQuery primeiro, depois Popper.js, depois Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script src="https://kit.fontawesome.com/11565cb3bb.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-maskmoney/3.0.2/jquery.maskMoney.min.js"></script>
    <script>
        //----------APLICA A MASCARA DE DINHEIRO NO CAMPOS VALOR DO PRODUTO----------
        $(function() {
            $('#valor').maskMoney();
        });
    </script>
</body>
</html>