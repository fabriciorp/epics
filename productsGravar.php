<?php
include('includes/conexao.php');
//---------------------------------------------
$grava = true;
$destino = '';
//----------ID DO PRODUTO----------
$codigo = (isset($_REQUEST['id']))  ? anti_sql($_REQUEST['id']) : 0;
//----------AÇÃO DO FORMULÁRIO----------
$acao = 0+anti_sql($_REQUEST['acao']);
//----------AÇÃO 1 = INCLUSÃO----------AÇÃO 2 = EDIÇÃO----------AÇÃO 3 = EXCLUSÃO----------
if($acao == 1 || $acao == 2){//----------SE AÇÃO FOR IGUAL A 1 OU 2 ENTÃO RECEBE OS DADOS DO FORMULÁRIO----------
    //----------CAMPOS DO FORMULÁRIO----------
    $nome = anti_sql($_REQUEST['nome']);//----------ARMAZENA DADOS DO CAMPO NOME NA VARIÁVEL----------
    $categoria = anti_sql($_REQUEST['categoria']);//----------ARMAZENA DADOS DO CAMPO CATEGORIA NA VARIÁVEL----------
    $cliente_id = 0+anti_sql($_REQUEST['cliente_id']);//----------ARMAZENA DADOS DO CAMPO ID DO CLIENTE NA VARIÁVEL----------
    $valor = (0 + str_replace("R$ ", "",str_replace(",", ".", str_replace(".", "", anti_sql($_REQUEST['valor'])))));//----------FORMATA CORRETAMENTE E ARMAZENA DADOS DO CAMPO VALOR NA VARIÁVEL----------
    $foto = $_FILES['foto'];//----------ARMAZENA DADOS DO CAMPO FOTO NA VARIÁVEL----------
    //---------------------------------------------
    if($nome == ""){//----------VERIFICA SE A VARIÁVEL DO CAMPO NOME ESTÁ VAZIA----------
        $mensagem = "O campo Nome &eacute; obrigat&oacute;rio";
        $grava = false;
    }
    if($categoria == ""){//----------VERIFICA SE A VARIÁVEL DO CAMPO CATEGORIA ESTÁ VAZIA----------
        $mensagem = "O campo Categoria &eacute; obrigat&oacute;rio";
        $grava = false;
    }
    if($cliente_id == ""){//----------VERIFICA SE A VARIÁVEL DO CAMPO ID DO CLIENTE ESTÁ VAZIA----------
        $mensagem = "O campo ID do Cliente &eacute; obrigat&oacute;rio";
        $grava = false;
    }
    if($valor <= 0){//----------VERIFICA SE A VARIÁVEL DO CAMPO VALOR É MAIOR QUE ZERO----------
        $mensagem = "O campo Valor &eacute; obrigat&oacute;rio";
        $grava = false;
    }
    if($grava){//----------VERIFICA SE OS DADOS DO FORMULÁRIOS ESTÃO CORRETOS----------
        if(!file_exists("images")){//----------VERIFICA SE A PASTA PARA ARMAZENAR AS THUMBS EXISTE----------
            mkdir("images");//----------CRIA A PASTA PARA ARMAZENAR AS THUMBS----------
        }
        if(!file_exists("images/products")){//----------VERIFICA SE A PASTA PARA ARMAZENAR AS THUMBS EXISTE----------
            mkdir("images/products");//----------CRIA A PASTA PARA ARMAZENAR AS THUMBS----------
        }
        if($acao == 1){//----------AÇÃO 1 = INCLUSÃO----------
            $codigo = retorna_codigo($conexao,'products');//----------RETORNA O ID DO PRODUTO QUE SERÁ INCLUSO----------
            if(!file_exists("images/products/".$codigo)){//----------VERIFICA SE A PASTA DO PRODUTO PARA ARMAZENAR AS THUMBS EXISTE----------
                mkdir("images/products/".$codigo);//----------CRIA A PASTA DO PRODUTO PARA ARMAZENAR AS THUMBS----------
            }
            $nome_foto = "";
            if($foto['name'] != ""){//----------VERIFICA SE O CAMPO FOTO ESTÁ PREENCHIDO----------
                preg_match("/\.(bmp|BMP|png|PNG|gif|GIF|jpg|JPG|jpeg|JPEG){1}$/i",  $foto['name'], $ext);//----------VERIFICA SE A EXTENSÃO DO ARQUIVO É VÁLIDA----------
                $nome_foto = uniqid(time()).".".$ext[1];//----------CRIA NOME ÚNICO PARA A FOTO----------
                move_uploaded_file($foto['tmp_name'], "images/products/".$codigo."/" . $nome_foto);//----------ARMAZENA A FOTO NA PASTA DO PRODUTO----------
            }
            //----------INCLUSÃO DO PRODUTO NO BANCO DE DADOS----------
            $sql = "INSERT INTO products(name,thumb,category,client_id,price)VALUES('".$nome."','".$nome_foto."','".$categoria."',".$cliente_id.",".$valor.")";
            $inserir = mysqli_query($conexao,$sql) or die ($sql);
            $linhaAfetada = mysqli_affected_rows($conexao);
            
            //----------VERIFICA SE HOUVE INCLUSÃO DO PRODUTO NO BANCO DE DADOS----------
            if($linhaAfetada >= 1){
                $mensagem = "Produto incluído com sucesso!";
                $destino = "productsListar.php";
            }else{
                $grava = false;
                $mensagem ="Produto não foi incluído!";
                $destino = "productsListar.php";
            }
        }elseif($acao == 2){//----------AÇÃO 2 = ALTERAÇÃO----------
            //----------VERIFICA SE EXISTE O PRODUTO NO BANCO DE DADOS----------
            $sql = "SELECT * FROM products WHERE id = ".$codigo;
            $consulta = mysqli_query($conexao,$sql);
            $NRows = mysqli_num_rows($consulta);
            if($NRows > 0){
                $dados = mysqli_fetch_assoc($consulta);
                $jsonAntigo = json_encode($dados);//----------ARMAZENA NA VARIÁVEL O JSON DOS DADOS ANTES DA ALTERAÇÃO----------

                $foto_atual = $dados['thumb'];//----------ARMAZENA NA VARIÁVEL O NOME DA FOTO ANTIGA----------
                if($foto['name'] != ""){//----------VERIFICA SE O CAMPO FOTO ESTÁ PREENCHIDO----------
                    if($foto_atual != "" && file_exists("images/products/".$codigo."/".$foto_atual)){//----------VERIFICA SE EXISTE A FOTO ANTIGA NO BANCO DE DADOS E NA PASTA----------
                        unlink("images/products/".$codigo."/".$foto_atual);//----------EXCLUSÃO DA IMAGEM NA PASTA DO PRODUTO----------
                    }

                    preg_match("/\.(bmp|BMP|png|PNG|gif|GIF|jpg|JPG|jpeg|JPEG){1}$/i",  $foto['name'], $ext);	//----------VERIFICA SE A EXTENSÃO DO ARQUIVO É VÁLIDA----------
                    $nome_foto = uniqid(time()).".".$ext[1];//----------CRIA NOME ÚNICO PARA A FOTO----------
                    move_uploaded_file($foto['tmp_name'], "images/products/".$codigo."/" . $nome_foto);//----------ARMAZENA A FOTO NA PASTA DO PRODUTO----------
                }else{
                    $nome_foto = $foto_atual;//----------ARMAZENA NA VÁRIAVEL O NOME ANTIGO DA FOTO----------
                }
                //----------ARMAZENA NA VARIÁVEL O JSON DOS DADOS NOVOS DO PRODUTO----------
                $jsonEdicao = '{"id":"'.$codigo.'","name":"'.$nome.'","thumb":"'.$nome_foto.'","category":"'.$categoria.'","client_id":"'.$cliente_id.'","price":"'.$valor.'"}';
                
                //----------ALTERAÇÃO DO PRODUTO NO BANCO DE DADOS----------
                $sql = "UPDATE products SET name='".$nome."',thumb='".$nome_foto."',category='".$categoria."',client_id=".$cliente_id.",price=".$valor."  WHERE id=".$codigo;
                $alterar = mysqli_query($conexao,$sql) or die ($sql);
                //----------INCLUSÃO DO LOG DE PRODUTO NO BANCO DE DADOS----------
                $sqlLog = "INSERT INTO products_logs(product_id,data_old,data_new)VALUES(".$codigo.",'".$jsonAntigo."','".$jsonEdicao."')";
                $inserir = mysqli_query($conexao,$sqlLog) or die ($sqlLog);

                $mensagem = "Produto alterado com sucesso!";
                $destino = "productsListar.php";
            }else{
                $grava = false;
                $mensagem = "Produto não foi alterado!";
                $destino = "productsListar.php";
            }
        }
    }
}elseif($acao == 3){//----------AÇÃO 3 = EXCLUSÃO----------
    //----------VERIFICA SE EXISTE O PRODUTO NO BANCO DE DADOS----------
    $sql = "SELECT * FROM products WHERE id = ".$codigo;
    $consulta = mysqli_query($conexao,$sql) or die ($sql);
    $NRows = mysqli_num_rows($consulta);
    if($NRows > 0){
        $dados = mysqli_fetch_array($consulta);

        if(is_dir("images/products/".$codigo)){//----------VERIFICA SE EXISTE A PASTA ONDE ARMAZENA A FOTO DO PRODUTO----------
            recursiveDelete("images/products/".$codigo);//----------EXCLUSÃO DA PASTA ONDE ARMAZENA A FOTO DO PRODUTO----------
        }
        //----------EXCLUSÃO DOS LOGS DO PRODUTO NO BANCO DE DADOS----------
        $sqlLog = "DELETE FROM products_logs WHERE product_id = ".$codigo;
        $excluir = mysqli_query($conexao,$sqlLog) or die ($sqlLog);

        //----------EXCLUSÃO DO PRODUTO NO BANCO DE DADOS----------
        $sql = "DELETE FROM products WHERE id = ".$codigo;
        $excluir = mysqli_query($conexao,$sql) or die ($sql);

        //----------VERIFICA SE HOUVE EXCLUSÃO DO PRODUTO NO BANCO DE DADOS----------
        $linhaAfetada = mysqli_affected_rows($conexao);
        if($linhaAfetada >= 1){
            $mensagem = "Produto excluído com sucesso!";
            $destino = "productsListar.php";
        }else{
            $grava = false;
            $mensagem = "Produto não foi excluído!";
            $destino = "productsListar.php";
        }
    }
}
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Grava Produtos - Teste EPICS</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
    <div class="container-fluid mt-3">
        <div class="row justify-content-around align-items-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-sm-6">
                                <h3 class="card-title">
                                    Gravação de Produtos
                                </h3>
                            </div>
                            <div class="col-sm-6">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb float-sm-right">
                                        <li class="breadcrumb-item">
                                            <a href="index.php" title="Home">Home</a>
                                        </li>
                                        <li class="breadcrumb-item">
                                            <a href="productsListar.php" title="Lista de Produtos">Lista de Produtos</a>
                                        </li>
                                        <li class="breadcrumb-item active">
                                            Grava de Produtos
                                        </li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <?php
                        //----------VERIFICA SE HOUVE AS MODIFICAÇÕES NO BANCO DE DADOS FORAM REALIZADAS----------
                        if($grava){//----------MENSAGEM DE SUCESSO----------
                        ?>
                            <div class="alert alert-success text-center" role="alert">
                                <i class="far fa-check-circle" style="font-size:50px;"></i><br><br>
                                <h4><?php echo $mensagem;?></h4><br>
                                Aguarde você está sendo redirecionado...
                            </div>
                        <?php
                        }else{//----------MENSAGEM DE ERRO----------
                        ?>
                            <div class="alert alert-danger text-center" role="alert">
                                <i class="far fa-times-circle" style="font-size:50px;"></i><br><br>
                                <h4><?php echo $mensagem;?></h4><br>
                                Aguarde você está sendo redirecionado...
                            </div>
                        <?php
                        }
                        if($destino != ''){//----------RETORNA PARA A PÁGINA REQUISITADA APÓS 2 SEGUNDOS----------
                            echo "<script>setTimeout(\"location.href='".$destino."'\",2000);</script>";
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- jQuery primeiro, depois Popper.js, depois Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script src="https://kit.fontawesome.com/11565cb3bb.js" crossorigin="anonymous"></script>
</body>
</html>