<?php
include('includes/conexao.php');

$codigo = 0+anti_sql($_REQUEST['id']);//----------ARMAZENA NA VARIÁVEL O ID DO PRODUTO----------
//----------VERIFICA SE EXISTE O PRODUTO NO BANCO DE DADOS----------
$sql = "SELECT * FROM products WHERE id = ".$codigo;
$resultados = mysqli_query($conexao,$sql) or die ($sql);
$NRows = mysqli_num_rows($resultados);

if($NRows > 0){
    $campos = mysqli_fetch_array($resultados);
    $folder = '';
    if($campos['thumb'] != ''){//----------VERIFICA SE O CAMPO THUMB ESTÁ PREENCHIDO----------
        $folder = 'images/products/'.$campos['id'].'/'.$campos['thumb'];//----------ARMAZENA NA VARIÁVEL O CAMINHO DA FOTO DO PRODUTO----------
    }

    //----------VERIFICA SE EXISTE LOGS DO PRODUTO NO BANCO DE DADOS----------
    $sqlLog = "SELECT * FROM products_logs WHERE product_id = ".$codigo." ORDER BY (id) DESC LIMIT 5";
    $resultadosLog = mysqli_query($conexao,$sqlLog) or die ($sqlLog);
    $NRowsLog = mysqli_num_rows($resultadosLog);
}else{
    echo "<script>setTimeout(location.href='productsListar.php', 0)</script>";
}
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Consulta de Produtos - Teste EPICS</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
    <div class="container-fluid mt-3">
        <div class="row justify-content-around align-items-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-sm-6">
                                <h3 class="card-title">
                                    Consulta de Produtos
                                </h3>
                            </div>
                            <div class="col-sm-6">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb float-sm-right">
                                        <li class="breadcrumb-item">
                                            <a href="index.php" title="Home">Home</a>
                                        </li>
                                        <li class="breadcrumb-item">
                                            <a href="productsListar.php" title="Lista de Produtos">Lista de Produtos</a>
                                        </li>
                                        <li class="breadcrumb-item active">
                                            Consulta de Produtos
                                        </li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-7">
                                <div class="form-group">
                                    <strong>Nome</strong><br>
                                    <?php echo $campos['name'];?>
                                </div>
                            </div>
                            <div class="col-sm-5">
                                <div class="form-group">
                                    <strong>Categoria</strong><br>
                                    <?php echo $campos['category'];?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <strong>ID do Cliente</strong><br>
                                    <?php echo $campos['client_id'];?>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <strong>Valor</strong><br>
                                    <?php echo 'R$ '.number_format($campos['price'],2,",",".");?>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <?php
                                if($folder != "" && file_exists($folder)){
                                ?>
                                    <strong>Foto</strong><br>
                                    <a href="<?php echo $folder;?>" target="_blank" title="Ver Foto Atual">
                                        <img src="<?php echo $folder;?>" class="img-fluid" alt="Foto Atual" title="Foto Atual">
                                    </a>
                                <?php
                                }else{
                                ?>
                                    <i class="fas fa-camera" title="Sem Imagem"></i>
                                <?php
                                }
                                ?>
                            </div>
                        </div>
                        <div class="row mt-4 border-top pt-3">
                            <div class="col-12">
                                <h5>Lista de Logs do Produto</h5>
                            </div>
                            <div class="col-12">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th width='20' class="align-middle text-center">ID</th>
                                            <th width='50' class="align-middle text-center">ID Prod.</th>
                                            <th class="align-middle">Dados Novos / Dados Antigos</th>
                                        </tr>
                                    </thead>
                                    <tbody>    
                                        <?php
                                        if($NRowsLog > 0)
                                        {
                                            while($camposLog = mysqli_fetch_array($resultadosLog)){
                                            ?>
                                            <tr>
                                                <td class="align-middle text-center">
                                                    <?php echo $camposLog['id'];?>
                                                </td>
                                                <td class="align-middle text-center">
                                                    <?php echo $camposLog['product_id'];?>
                                                </td>
                                                <td class="align-middle">
                                                    <strong>Dados Novos:</strong>
                                                    <?php echo $camposLog['data_new'];?>
                                                    <br>
                                                    <strong>Dados Antigos:</strong>
                                                    <?php echo $camposLog['data_old'];?>
                                                </td>
                                            </tr>
                                            <?php
                                            }
                                        }else{
                                        ?>
                                            <tr>
                                                <td colspan="6" class="align-middle text-center text-bold">
                                                    Nenhum Registro encontrado
                                                </td>
                                            </tr>
                                        <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- jQuery primeiro, depois Popper.js, depois Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script src="https://kit.fontawesome.com/11565cb3bb.js" crossorigin="anonymous"></script>
</body>
</html>