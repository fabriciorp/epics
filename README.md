
**TESTE EPICS - FABRICIO JONATAS DE MORAES**

--

**Para executar o teste, basta estar com um servidor local com suporte a PHP 7.x e MySQL**

1. Antes de Iniciar o teste vá até o arquivo **conexao.php** que está dentro da pasta **includes** e verifique se o nome do banco que deixei como padrão **(Nome Padrão: teste_epics_fjm)** poderá ser utilizado na sua máquina para que não tenha conflito, pois, ao executar pela primeira o sistema ele irá verificar se existe algum outro banco de dados com o mesmo nome e excluí-lo a fim de evitar conflitos durante a execução.

--

**Para iniciar o teste, abra o arquivo index.php**