<?php
include('includes/conexao.php');
//----------CONFIGURANDO A PAGINAÇÃO----------
$QUANTIDADE_PAGINA = 5;//----------ARMAZENA NA VARIÁVEL A QNT. DE ITENS QUE TERÁ POR PÁGINA----------

if(isset($_REQUEST['pagina'])){//----------VERIFICA SE EXISTE O REQUEST DA PÁGINA----------
	$pagina = anti_sql($_REQUEST['pagina']);//----------ARMAZENA NA VARIÁVEL A PÁGINA SOLICITADA----------
}else{
	$pagina = "";
}
if($pagina == ""){
	$inicio = 0;
	$pagina = 1;
}else{//----------CONFIGURANDO O PONTO DE INÍCIO DOS LOGS NA PÁGINA REQUISITADA----------
	$inicio = ($pagina - 1) * $QUANTIDADE_PAGINA;
}
//----------CONFIGURANDO OS FILTROS----------
$criterios = "";

$id = isset($_REQUEST['id']) ? 0+anti_sql($_REQUEST['id']) : 0; //----------ARMAZENA NA VARIÁVEL O ID DO PRODUTO FILTRADO----------
if(isset($id) && $id > 0){//----------VERIFICA SE O ID DO PRODUTO EXISTE E É MAIOR QUE ZERO----------
	$criterios = ($criterios == "" ? " WHERE " : $criterios." AND ")." product_id = ".$id." ";//----------ADICIONA O FILTRO POR ID DO PRODUTO NA CONSULTA DE LOGS NO BANCO DE DADOS----------
}

$paginacao =  " LIMIT " . $inicio . "," . $QUANTIDADE_PAGINA;//----------CONFIGURANDO A PAGINAÇÃO NA CONSULTA DE LOGS NO BANCO DE DADOS----------

$sql = "SELECT * FROM products_logs ".$criterios." ORDER BY (id) DESC";

$nrows_total = mysqli_num_rows(mysqli_query($conexao,$sql));//----------PEGANDO A QNT. TOTAL DE REGISTROS PARA CONTROLAR A PAGINAÇÃO----------

$sql = $sql.$paginacao;//----------ADICIONA A PAGINAÇÃO NA CONSULTA DO BANCO DE DADOS----------

$resultados = mysqli_query($conexao,$sql);
$nrows_atual = $resultados != null ? mysqli_num_rows($resultados) : 0;//----------PEGANDO A QNT. DE REGISTROS REFERENTE A CONSULTA ATUAL PARA CONTROLAR A LISTAGEM----------

$total_paginas = ceil($nrows_total/$QUANTIDADE_PAGINA);//----------ARMAZENA NA VARIÁVEL O TOTAL DE PÁGINAS QUE IRÃO APARECER NA PAGINAÇÃO----------
//----------VARIÁVEL UTILIZADA NA PAGINAÇÃO PARA NÃO PERDER OS PARÂMETROS DE FILTRO----------
$strLink = "productsLogsListar.php?";
$strLink .= "id=" . $id;
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Lista de Logs de Produtos - Teste EPICS</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
    <div class="container-fluid mt-3">
        <div class="row justify-content-around align-items-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-sm-6">
                                <h3 class="card-title">
                                    Lista de Logs de Produtos
                                </h3>
                            </div>
                            <div class="col-sm-6">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb float-sm-right">
                                        <li class="breadcrumb-item">
                                            <a href="index.php" title="Home">Home</a>
                                        </li>
                                        <li class="breadcrumb-item">
                                            <a href="productsListar.php" title="Lista de Produtos">Lista de Produtos</a>
                                        </li>
                                        <li class="breadcrumb-item active">
                                            Lista de Logs de Produtos
                                        </li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th width='20' class="align-middle text-center">ID</th>
                                    <th width='50' class="align-middle text-center">ID Prod.</th>
                                    <th class="align-middle">Dados Novos / Dados Antigos</th>
                                </tr>
                            </thead>
                            <tbody>    
                                <?php
                                if($nrows_atual > 0)
                                {
                                    while($campos = mysqli_fetch_array($resultados)){
                                    ?>
                                        <tr>
                                            <td class="align-middle text-center">
                                                <?php echo $campos['id'];?>
                                            </td>
                                            <td class="align-middle text-center">
                                                <?php echo $campos['product_id'];?>
                                            </td>
                                            <td class="align-middle">
                                                <strong>Dados Novos:</strong>
                                                <?php echo $campos['data_new'];?>
                                                <br>
                                                <strong>Dados Antigos:</strong>
                                                <?php echo $campos['data_old'];?>
                                            </td>
                                        </tr>
                                    <?php
                                    }
                                }else{
                                ?>
                                    <tr>
                                        <td colspan="6" class="align-middle text-center text-bold">
                                            Nenhum Registro encontrado
                                        </td>
                                    </tr>
                                <?php
                                }
                                if($total_paginas > 1){//----------VERIFICA SE O TOTAL DE PAGINAS É MAIOR QUE A LISTAGEM ATUAL----------
                                ?>
                                    <tr>
                                        <td colspan="6" class="align-middle text-center text-bold">
                                            <?php paginar_registros($total_paginas, $pagina, 10, "btn btn-primary", $strLink);?>
                                        </td>
                                    </tr>
                                <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- jQuery primeiro, depois Popper.js, depois Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script src="https://kit.fontawesome.com/11565cb3bb.js" crossorigin="anonymous"></script>
</body>
</html>