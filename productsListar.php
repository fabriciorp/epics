<?php
include('includes/conexao.php');
//----------VERIFICA SE EXISTEM PRODUTOS NO BANCO DE DADOS----------
$sql = "SELECT * FROM products ORDER BY (id) DESC";
$resultados = mysqli_query($conexao,$sql) or die ($sql);
$NRows = $resultados != null ? mysqli_num_rows($resultados) : 0;
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Lista de Produtos - Teste EPICS</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <style>
        a{/*----------RETIRANDO UNDERLINE DE TODAS AS TAGS a DA PÁGINA----------*/
            text-decoration: none !important;
        }
    </style>
</head>
<body>
    <div class="container-fluid mt-3">
        <div class="row justify-content-around align-items-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-sm-6">
                                <h3 class="card-title">
                                    Lista de Produtos
                                </h3>
                            </div>
                            <div class="col-sm-6">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb float-sm-right">
                                        <li class="breadcrumb-item">
                                            <a href="index.php" title="Home">Home</a>
                                        </li>
                                        <li class="breadcrumb-item active">
                                            Lista de Produtos
                                        </li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 text-right">
                                <a href="productsIncluir.php" title="Adicionar Produto">
                                    <button type="button" class="btn btn-primary">
                                        <i class="fas fa-plus"></i> Adicionar
                                    </button>
                                </a>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th width='20' class="text-center">ID</th>
                                    <th>Nome</th>
                                    <th width='50'>Foto</th>
                                    <th>Categoria</th>
                                    <th>ID do Cliente</th>
                                    <th>Preço</th>
                                    <th width='220' class="text-center">Ações</th>
                                </tr>
                            </thead>
                            <tbody>    
                                <?php
                                if($NRows > 0)
                                {
                                    while($campos = mysqli_fetch_array($resultados)){
                                        $folder = '';
                                        if($campos['thumb'] != ''){//----------VERIFICA SE O CAMPO THUMB ESTÁ PREENCHIDO----------
                                            $folder = 'images/products/'.$campos['id'].'/'.$campos['thumb'];//----------ARMAZENA NA VARIÁVEL O CAMINHO DA FOTO DO PRODUTO----------
                                        }
                                    ?>
                                    <tr>
                                        <td class="align-middle text-center">
                                            <?php echo $campos['id'];?>
                                        </td>
                                        <td class="align-middle">
                                            <?php echo $campos['name'];?>
                                        </td>
                                        <td class="align-middle text-center">
                                            <?php
                                            if($folder != "" && file_exists($folder)){
                                            ?>
                                                <a href="<?php echo $folder;?>" target="_blank" title="Ver Foto Atual">
                                                    <img src="<?php echo $folder;?>" class="img-fluid" alt="Foto Atual" title="Foto Atual">
                                                </a>
                                            <?php
                                            }else{
                                            ?>
                                                <i class="fas fa-camera" title="Sem Imagem"></i>
                                            <?php
                                            }
                                            ?>
                                        </td>
                                        <td class="align-middle">
                                            <?php echo $campos['category'];?>
                                        </td>
                                        <td class="align-middle">
                                            <?php echo $campos['client_id'];?>
                                        </td>
                                        <td class="align-middle">
                                            <?php echo 'R$ '.number_format($campos['price'],2,",",".");?>
                                        </td>
                                        <td class="align-middle text-center">
                                            <a href="productsLogsListar.php?id=<?php echo $campos['id'];?>" title="Ver Logs do Produto">
                                                <button type="button" class="btn btn-success">
                                                    <i class="fas fa-history"></i>
                                                </button>
                                            </a>
                                            <a href="productsConsultar.php?id=<?php echo $campos['id'];?>" title="Consultar Produto">
                                                <button type="button" class="btn btn-primary">
                                                    <i class="fas fa-eye"></i>
                                                </button>
                                            </a>
                                            <a href="productsAlterar.php?id=<?php echo $campos['id'];?>" title="Editar Produto">
                                                <button type="button" class="btn btn-secondary">
                                                    <i class="fas fa-edit"></i>
                                                </button>
                                            </a>
                                            <a href="productsGravar.php?acao=3&id=<?php echo $campos['id'];?>" title="Excluir Produto">
                                                <button type="button" class="btn btn-danger">
                                                    <i class="fas fa-times-circle"></i>
                                                </button>
                                            </a>
                                        </td>
                                    </tr>
                                    <?php
                                    }
                                }else{
                                ?>
                                    <tr>
                                        <td colspan="7" class="align-middle text-center text-bold">
                                            Nenhum Registro encontrado
                                        </td>
                                    </tr>
                                <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- jQuery primeiro, depois Popper.js, depois Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script src="https://kit.fontawesome.com/11565cb3bb.js" crossorigin="anonymous"></script>
</body>
</html>