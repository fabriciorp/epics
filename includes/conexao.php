<?php
$nomeBD = 'teste_epics_fjm';

$servername = "localhost";//----------NOME DO SERVIDOR - PADRÃO localhost----------
$database = $nomeBD;//----------NOME DO BANCO DE DADOS----------
$username = "root";//----------NOME DO USUÁRIO - PADRÃO root----------
$password = "";//----------SENHA DE CONEXÃO DO BANCO DE DADOS----------

//----------CRIANDO CONEXÃO----------
$conexaoServer = mysqli_connect($servername,$username,$password);
if($conexaoServer){
    $conexaoBD = mysqli_select_db($conexaoServer, $database);
    if($conexaoBD){
        $conexao = mysqli_connect($servername, $username, $password, $database);
    }
}else{
    echo 'Sem conexão com o servidor';
}
include('funcoes.php');
//----------VERIFICA SE EXISTE O BANCO DE DADOS----------
if(!$conexaoBD){
    if(is_dir("images/products")){//----------VERIFICA SE EXISTE A PASTA ONDE ARMAZENA AS FOTOS DOS PRODUTOS----------
        recursiveDelete("images/products");//----------EXCLUSÃO DA PASTA ONDE ARMAZENA AS FOTOS DOS PRODUTOS----------
    }
    //----------VERIFICA SE EXISTE ALGUM BANCO DE DADOS COM O MESMO NOME E EXCLUI ELE----------
    $sqlExcluiBD = "DROP DATABASE IF EXISTS `".$nomeBD."`";
    $resultaExcluiBD = mysqli_query($conexaoServer,$sqlExcluiBD) or die($sqlExcluiBD);
    
    //----------CRIA O NOVO BANCO DE DADOS----------
    $sqlCriaBD = "CREATE DATABASE `".$nomeBD."`";
    $resultaCriaBD = mysqli_query($conexaoServer,$sqlCriaBD) or die($sqlCriaBD);
    
    
    //----------ARMAZENA EM VARIÁVEIS OS DADOS PARA CONEXÃO DO BANCO DE DADOS CRIADO----------
    $servername = "localhost";//----------NOME DO SERVIDOR - PADRÃO localhost----------
    $database = $nomeBD;//----------NOME DO BANCO DE DADOS----------
    $username = "root";//----------NOME DO USUÁRIO - PADRÃO root----------
    $password = "";//----------SENHA DE CONEXÃO DO BANCO DE DADOS----------
    //----------CRIANDO CONEXÃO----------
    $conexaoNova = mysqli_connect($servername, $username, $password, $database);

    //----------CRIA A TABELA products NO BANCO DE DADOS----------
    $sqlCriaTBProducts = "CREATE TABLE `products` (
        `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
        `name` varchar(50) NOT NULL,
        `thumb` varchar(100) NOT NULL,
        `category` varchar(25) NOT NULL,
        `client_id` int(11) NOT NULL,
        `price` float(10,2) NOT NULL
      ) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
    $resultaCriaCriaTBProducts = mysqli_query($conexaoNova,$sqlCriaTBProducts) or die($sqlCriaTBProducts);
    
    //----------CRIA A TABELA products_logs NO BANCO DE DADOS----------
    $sqlCriaTBProductsLogs = "CREATE TABLE `products_logs` (
        `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
        `product_id` int(11) NOT NULL,
        `data_old` text NOT NULL,
        `data_new` text NOT NULL
      ) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
    $resultaCriaTBProductsLogs = mysqli_query($conexaoNova,$sqlCriaTBProductsLogs) or die($sqlCriaTBProductsLogs);
?>
    <h6>
        Essa é a primeira vez que você acessa o Teste Epics.
    </h6>
    <p>Por esse motivo criamos um novo banco de dados para que você possa utilizar o Teste Epics corretamente!</p>
<?php
}
?>