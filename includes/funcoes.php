<?php
//----------FUNÇÃO QUE RETORNA O VALOR DE UMA DETERMINADA CONSULTA NO BANCO DE DADOS----------
function retorna_valor($conexao,$SqlCampo){
	$sql = $SqlCampo;
	$qsql = mysqli_query($conexao,$sql);
	$asql = mysqli_fetch_array($qsql);

	if(!empty($asql)){
		return $asql[0];
	}
}
//----------FUNÇÃO QUE RETORNA O RETORNA O PRÓXIMO ID EM UMA DETERMINADA TABELA DO BANCO DE DADOS----------
function retorna_codigo($conexao,$tabela){
	$sql = "SHOW TABLE STATUS LIKE '$tabela'";
	$resultado = mysqli_query($conexao,$sql);

	$linha = mysqli_fetch_array($resultado);
	return $linha['Auto_increment'];
}
//----------FUNÇÃO QUE RETIRA DA VARIÁVEL QUALQUER TENTATIVA DE SQL INJECTION----------
function anti_sql($texto){
	//----------LISTA DE PALAVRAS PARA PROCURAR----------
	$check[1] = chr(34);//----------SÍMBOLO "----------
	$check[2] = chr(39);//----------SÍMBOLO '----------
	$check[3] = chr(92);//----------SÍMBOLO /----------
	$check[4] = chr(96);//----------SÍMBOLO `----------
	$check[5] = "drop table";
	$check[6] = "update";
	$check[7] = "alter table";
	$check[8] = "drop database";	
	$check[9] = "drop";
	$check[10] = "select";
	$check[11] = "delete";
	$check[12] = "insert";
	$check[13] = "alter";
	$check[14] = "destroy";
	$check[15] = "table";
	$check[16] = "database";
	$check[17] = "union";
	$check[18] = "TABLE_NAME";
	$check[19] = "1=1";
	$check[20] = 'or 1';
	$check[21] = 'exec';
	$check[22] = 'INFORMATION_SCHEMA';
	$check[23] = 'like';
	$check[24] = 'COLUMNS';
	$check[25] = 'into';
	$check[26] = 'VALUES';
	$check[27] = "DROP TABLE";
	$check[28] = "UPDATE";
	$check[29] = "ALTER TABLE";
	$check[30] = "DROP DATABASE";	
	$check[31] = "DROP";
	$check[32] = "SELECT";
	$check[33] = "DELETE";
	$check[34] = "INSERT";
	$check[35] = "ALTER";
	$check[36] = "DESTROY";
	$check[37] = "TABLE";
	$check[38] = "DATABASE";
	$check[39] = "UNION";
	$check[40] = "table_name";
	$check[41] = "1=1";
	$check[42] = 'OR 1';
	$check[43] = 'EXEC';
	$check[44] = 'information_schema';
	$check[45] = 'LIKE';
	$check[46] = 'columns';
	$check[47] = 'INTO';
	$check[48] = 'values';
	$check[49] = 'from';
	$check[50] = 'FROM';
	
	//----------CRIA-SE AS VARIÁVEIS PARA CONTROLE DO LOOP QUE FARÁ A BUSCA E SUBSTITUIÇÃO----------
	$y = 1;
	$x = sizeof($check);
	//----------FAZ-SE O LOOP PROCURANDO ALGUMAS DAS PALAVRAS ESPECIFICADAS ACIMA, CASO ENCONTRE ALGUMA DELAS, ESTE CÓDIGO SUBSTITUIRÁ POR UM ESPAÇO EM BRANCO----------
	while($y <= $x){
		   $target = strpos($texto,$check[$y]);
			if($target !== false){
				$texto = str_replace($check[$y], "", $texto);
			}
		$y++;
	}//----------RETORNA A VARIÁVEL LIMPA SEM PERIGOS DE SQL INJECTION----------
	return $texto;
}
//----------FUNÇÃO QUE MONTA OS BOTÕES DE PAGINAÇÃO CONFORME A LISTAGEM REQUISITADA----------
function paginar_registros($intQtdPaginas, $intPagAtual, $intQtdPagExibir, $strCSS, $strLink){
	$PagInicio = 0;
	$PagFim = 0;
	
	if($intQtdPaginas <= $intQtdPagExibir){
		$PagInicio = 1;
		$PagFim = $intQtdPaginas;
	}else{
		if($intPagAtual <= ceil($intQtdPagExibir / 2)){
			$PagInicio = 1;
			$PagFim = $intQtdPagExibir;
		}elseif($intPagAtual > $intQtdPaginas - ceil($intQtdPagExibir / 2)){
			$PagInicio = $intQtdPaginas - ($intQtdPagExibir - 1);
			$PagFim = $intQtdPaginas;
		}else{
			$PagInicio = $intPagAtual - ceil($intQtdPagExibir / 2);
			$PagFim = $intPagAtual + ceil($intQtdPagExibir / 2);
		}
	}
	
	echo "<input name=btnPrevious type=button class='" . $strCSS . "' value='<< anterior' title='Página Anterior' onClick=location='" . $strLink . "&pagina=" . ($intPagAtual - 1) . "' " . ($intPagAtual == 1 ? "disabled" : "style='cursor:pointer; cursor:hand;'") . "> ";	
	echo "<input name=Parentese1 type=button class='" . $strCSS . "' value='['> ";
	
	//----------LOOP DE BOTÕES COM O NUMERO DAS PAGINAS EXISTENTE----------
	for($p = $PagInicio; $p <= $PagFim; $p++){
		if ($p == $intPagAtual){//----------SE A PAGINA DO LOOP FOR IGUAL A PAGINA ATUAL DEIXA O BOTÃO DESABILITADO----------
			echo "<input name=PagAtual type=button class='" . $strCSS . "' value=" . $p . " disabled> ";
		}else{
			echo "<input style='cursor:pointer; cursor:hand;' name='" . $p . "' type='button' class='" . $strCSS . "' value='" . $p . "' onClick=location='" . $strLink . "&pagina=" . $p . "'> ";
		}
	}
	
	echo "<input name='Parentese2' type='button' class='" . $strCSS . "' value=']'> ";
	echo "<input name='btnNext' type='button' class='" . $strCSS . "' value='pr&oacute;ximo >>' title='Próxima Página' onClick=location='" . $strLink . "&pagina=" . ($intPagAtual + 1) . "' " . ($intPagAtual == $intQtdPaginas ? "disabled" : "style='cursor:pointer; cursor:hand;'") . "> ";
}
//----------FUNÇÃO QUE EXCLUI UMA DETERMINADA PASTA DO PROJETO----------
function recursiveDelete($dir){
	if($handle = @opendir($dir)){
		while(($file = readdir($handle)) !== false){
			if(($file == ".") || ($file == "..")){
				continue;
			}
			if(is_dir($dir.'/'.$file)){
				recursiveDelete($dir.'/'.$file);
			}else{
				unlink($dir.'/'.$file); 
			}
		}
	@closedir($handle);
		rmdir ($dir);
	}
}
?>